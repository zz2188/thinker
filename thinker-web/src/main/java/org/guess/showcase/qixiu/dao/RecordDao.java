package org.guess.showcase.qixiu.dao;

import org.guess.core.orm.EntityDao;
import org.guess.showcase.qixiu.model.Record;

/**
 * 记录dao
 * @author rguess
 * @version 2014-06-04
 */
public interface RecordDao extends EntityDao<Record, Long>{
	
}
